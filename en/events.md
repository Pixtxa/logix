---
title: Events
---
<!-- Please keep ongoing and current events in sync with _index.md -->
- Upcoming:
  - card10 [workout in Berlin](/en/workouts/berlin-2019-11-09): xHain, Sat-Sun, November 9th to 10th, 2019


<!-- past events, please remove any events you move here from _index.md to keep the home page clean -->
- Previous
  - **card10 reworking/soldering/firmware/etc.**: card10 village (R8); for firmware work, please bring your laptop with a clone of the firmware git and the [toolchain](firmware.card10.badge.events.ccc.de/how-to-build.html)
  - card10 Soldering Session in Berlin: xHain Wed-Thurs 14&15. August starting at 10 am
  - card10 Soldering Session in Berlin: xHain Sat-Sun 10.-11. August starting at 10 am
  - card10 meetup in Berlin: xHain 06.08. ~~19:00~~ => 20:00
  - card10 meetup in Berlin: xHain 05.06. 20:00
  - card10 meetup in Berlin: afra 21.06. 19:00
  - card10 meetup in Berlin: xHain 14.06. 19:00
  - card10 at the [GPN19](https://entropia.de/GPN19:There_will_have_been_a_camp_badge:_We%27re_reconstructing_future_technology_and_you_can_help_us_with_this_mission)
  - CCCamp2019 Badge Workshop at #eh19: [Description](https://conference.c3w.at/eh19/talk/VHFMJ7/), 2019-04-21, 14:30–15:30
  - CCCamp 2019 Badge at #eh19 (easterhegg2019): [Description](https://conference.c3w.at/eh19/talk/DA7KTT/), [Recording](https://media.ccc.de/search/?q=CCCamp+Badge+Talk)
  - **card10 [talk](https://fahrplan.events.ccc.de/camp/2019/Fahrplan/events/10365.html)**: Curie Wed 21.08. 12:00
  - **card10 firmware workshop**: card10 village (R8), Sun 18.08. 16:00
  - card10 [workout in Berlin](/en/workouts/berlin-2019-10-03): xHain, Thurs-Sun, October 3rd to 6th, 2019
