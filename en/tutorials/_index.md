---
title: Tutorials
---

## Getting started


## Micropython


## E-textile

* Photo tutorial for the E-textile card10 workshop at cccam19 by plusea https://flickr.com/photos/plusea/albums/72157710505658161

## ECG

* ECG kit assembly https://card10.badge.events.ccc.de/en/ecg_kit_assembly/

## Repairs

We dedicated a separate section of card10logix to repair work for the upkeep of your card10: https://card10.badge.events.ccc.de/en/upkeep/display/
