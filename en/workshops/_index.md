---
title: Workshops
---

### Participating
 * there is no sign-up, just turn up
 * check the worksop descriptions to see if you should bring anything/have anything pre-installed on your laptop
 * you might also find more card10 related sessions on the cccamp19 [self organised sessions page](https://events.ccc.de/camp/2019/wiki/Static:Self-organized_Sessions)

## past Workshops

### Micropython on card10 for complete beginners (DE + EN)
  * Time: Day 4, 15:00
  * Location: card10 area
  * bring a laptop and a usb-c cable
  * and your card10
  * no knowledge of python needed

### E-textile card10 workshop (DE + EN)
  * Time: Day 4, 18:30 - 20:30
  * Location: card10 area
  * bring: card10, laptop, USB-C cable
  * (EN) also useful: sewing needles, thread, through hole LEDs, needle nose pliers, snap fasteners
  * (DE) auch nuetzlich: naehnadeln, faden, 3mm oder 5mm LEDs (zum durchstecken), feine zangen zum draht biegen, druckknoepfe
   
  
### Pulsoxymetrie & ECG fuer Hacker (DE)
  * Time: Day 4, 21:00
  * Location: card10 area
  * ein Vortrag vom CERT

### ECG kit assembly
  * Time: Day 4, 20:30
  * Location: Hardware Hacking Area
  * ECG kits available at workshop and card10 assembly

### ECG kit assembly
  * Time: Day 4, 21:30
  * Location: Hardware Hacking Area
  * ECG kits available at workshop and card10 assembly



### Environmental and IMU sensor workshop
> ~~Time: Day 2, 19:00~~
> ~~Location: card10 village~~
  * Time: Day2, **20:00**
  * Location: Johnson
  * [Description](https://events.ccc.de/camp/2019/wiki/Session:Environmental_and_IMU_sensors_on_the_card10_badge)


### Rust l0dables on the card10 badge
  * Time: Day 2, 19:00
  * Location: Johnson
  * [Description](https://events.ccc.de/camp/2019/wiki/Session:Rust_l0dables_on_the_card10_badge)


### Micropython on card10 for complete beginners
  * Time: Day 3, 15:00
  * Location: card10 area
  * bring a laptop and a usb-c cable
  * and your card10
  * no knowledge of python needed

### E-textile card10 workshop
  * Time: Day 3, 19:00 - 20:30
  * Location: card10 area
  * bring: card10, laptop, USB-C cable
  * (EN) also useful: sewing needles, thread, through hole LEDs, needle nose pliers, snap fasteners
  * (DE) auch nuetzlich: naehnadeln, faden, 3mm oder 5mm LEDs (zum durchstecken), feine zangen zum draht biegen, druckknoepfe
 

### EKG fuer Hacker
  * Time: Day3, 21:00
  * Location: card10 area
  * EKG daten lesen praesentiert vom CERT