---
title: Workout Berlin 2019-11-09
---

The second card10 workout will happen at xHain in Berlin on the weekend from November 9th - 10th, 2019.

## Location and Times
- Location: [xHain](https://x-hain.de/en/page/about/): Grünberger Str. 16, 10243 Berlin
- Times:  2019-11-09 to 2019-11-10, all day in general, details to follow
- Capacity: Around 10-20 people

## People
- andreas
- rahix
- schneider
- spacecarrot
- YOU?

## Activities
### Firmware
We will work on integating more Merge Requests into the firmware, and hopefully create a new release.

### Hardware & Repair Cafe
If you have issues with your hardware swing by and we can see if we can fix it.  We will have spare parts!

### Software
We plan to experiment with some more phones to improve BLE support.  If you want to work on the wiki
feel free too.  Lots of people who know around ;)

### Wearware
We still have plenty of conductive thread and a lot of ideas.  Do you have some too?


## Events
We can make us of xHain during the times as mentioned above. If you want to allocate some time
for specific workshops or similar, mention it here:

Title | Person | Start Time / Day | Duration
------|--------|------------------|---------
Be the first! | | |

### MicroPython introduction
If you haven't dared trying to write your own apps for your card10, now is your chance to learn how
to do it! Programming your card10 using MicroPython is easy to learn, and all you need is your
laptop (preferably Linux, but Windows and MacOS work as well), a USB-C cable, and of course your
card10.

### App discovery
There are hundreds of card10 apps in the hatchery. Let's start the day by exchanging our favourite
discoveries, and trying some new ones.

### Repair cafe
Open hardware means you can fix it! We are bringing some spare parts and plenty of assistance is
around, so together we can restore the broken bits of some card10s.

Obviously, you should bring your card10 if you want to fix it ;), everything else will be around at
the event.

## Accomodation

xHain is not a place to sleep, so please make sure you have some accomodation. Get in touch with us
on IRC/Matrix if you don't know.
