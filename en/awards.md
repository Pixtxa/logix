---
title: Awards
---

Idea: From time to time hold small award ceremonies to keep people motivated to contribute
to the card10 project


## 36C3

We will host a small awards ceremony at 36C3.

### Categories
 - Best documented project
 - Best use of a sensor (ECG, Accelerometer, etc...)
 - Best LED application (e.g. POV)
 - Best accessibility contribution
 - Best interaction with 36C3


### Submitting a contribution

You submit a contribution (be it an app, project, documentation, etc..) by entering it in the list of contributions at the end of this section.

You can also reach out to us via twitter, mastodon, matrix and IRC. We will add your contribution for you if needed.


### Date and Time

Unless otherwise noted the ceremony will take place Day 2, 19:00 at the card10 assembly.


### Judging

Judging will be done by original contributors to the card10 project. For the accessibility category we will approach relevant teams at 36C3.

### Prizes

We are still collecting, but at least there will be a few r0kets and rad1os available.


### Contributions

 - Your contribution
